function [kd] = kdfourth(derivative)
    % Compute kd using delta^4 CCM.
    kd = -derivative -derivative*sqrt(1 + 16 * derivative^4);
end