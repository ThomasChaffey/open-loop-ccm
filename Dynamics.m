function [xdot] = Dynamics(xt, ut)
    
    xdot = xt + ut;
    
end