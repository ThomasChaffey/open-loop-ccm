# README - Open Loop CCM #

A MATLAB script to calculate an open loop control contraction metric controller.

### Overview ###

This script calculates an open loop CCM controller for a single dimensional system by 
discretising in time and along the initial curve c(t_i, s) (with respect to s).  Integration 
in time is performed using Range Kutta 4 and integration in space is performed using quadrature
and approximate derivatives.

### What are all the files? ###

main.m contains the main loop of code required to compute the forward image of c for 10 seconds 
(which includes the system response).

ComputeControl.m integrates the control signal along the curve c(t, s), using quadrature.

Dynamics.m gives the dynamics of the system in the form dot{x} = f(x, u).

kdfourth.m and kdsquared.m return kd computed for two different metrics and the example dynamics given in
Dynamics.m

RungeKutta4.m is a modified version of Ian Manchester's script to perform numerical integration by the
Runge Kutta method.
