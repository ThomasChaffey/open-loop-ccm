s_int = 0.01; % discretisation interval along geodesic
c0 = 0:s_int:1; % initial minimising geodesic
h = 0.01; % 100 Hz computation

ct = zeros(10/h + 1, length(c0)); % Array for forward image of c
u = zeros(10/h + 1, 1/s_int + 1); % Array for controls
ct(1, :) = c0;
u(1, :) = ComputeControl(c0, s_int, @kdfourth); % Compute initial control
t = 0;

for t = 1: 10/h + 1 % For 10 seconds, compute forward path and control
    c = ct(t, :);
    for i = 1:length(c)
        ct(t + 1, i) = RungeKutta4(@Dynamics, c(i), u(t, i), h);
    end
    u(t + 1, :) = ComputeControl(ct(t + 1, :), s_int, @kdfourth);
end

