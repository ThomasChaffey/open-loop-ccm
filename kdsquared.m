function [kd] = kdsquared(derivative)
    % Compute kd using delta^2 CCM
    kd = -derivative*(1 + sqrt(5));
end