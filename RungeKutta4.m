function [ xth ] = RungeKutta4(f, xt, ut, h)
% Simple evaluation of 4th order Runge Kutta. Written for AMME5520 project
% by Ian Manchester.

    k1 = f(xt,ut);
    k2 = f(xt+h/2*k1, ut);
    k3 = f(xt+h/2*k2, ut);
    k4 = f(xt+h*k3, ut);
    
    % multiply by h
    xth = xt+h*(k1+2*k2+2*k3+k4)/6;

end